﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour
{

	public GameObject FlashLeft;
	public GameObject FlashRight;

	private bool leftOn;
	private bool rightOn;

	public AudioClip OnMusicClip;
	public AudioSource OnMusicSource;
	public AudioClip OffMusicClip;
	public AudioSource OffMusicSource;

    // Start is called before the first frame update
    void Start()
    {
        OnMusicSource.clip = OnMusicClip;
		OffMusicSource.clip = OffMusicClip;
    }

    // Update is called once per frame
    void Update()
    {
        if(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch) > 0.75f){
			FlashLeft.SetActive(true);
			FlashRight.SetActive(false);
			OnMusicSource.Play();
		}
		if(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch) > 0.75f){
			FlashLeft.SetActive(false);
			FlashRight.SetActive(true);
			OnMusicSource.Play();
		}
		if(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch) < 0.75f && OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch) < 0.75f){
			FlashLeft.SetActive(false);
			FlashRight.SetActive(false);
			OffMusicSource.Play();
		}
    }
}
