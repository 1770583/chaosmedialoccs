﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusReady : MonoBehaviour
{

    private bool triggerPressed;
    private bool once;
    public mainMenu ScriptAttPret;
   

    // Start is called before the first frame update
    void Start()
    {
        triggerPressed = false;
        once = true;
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
        OVRInput.FixedUpdate();

        if (OVRInput.Get(OVRInput.Button.One) && once)
        {
            ScriptAttPret.PlayGame();
            once = false;
        }
    }
}
