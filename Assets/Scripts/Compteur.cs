﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Compteur : MonoBehaviour
{

    private float currentTime = 0f;
    private float startingTime;
	private float currentTimeMin;

    [SerializeField] TextMeshProUGUI countdownText;
	[SerializeField] int timeDown;
	[SerializeField] int startTime = 2359;
	[SerializeField] int endTime = 2400;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = startingTime;
    }

    // Update is called once per frame
    void Update()
    {
		if(endTime-1 >=startTime){
			currentTime += Time.deltaTime;
			if(currentTime>=59){
				startTime = endTime;
				currentTime=0;
			}
		}
		else{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
		}
        
        countdownText.text = startTime.ToString("00:00")+":"+currentTime.ToString("00")+" PM";
    }
}
