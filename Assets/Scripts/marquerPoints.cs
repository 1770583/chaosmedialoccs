﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class marquerPoints : MonoBehaviour
{


	public int scoreGardes = 20000;
	public int scoreVoleurs = 0;
	public TextMeshProUGUI argentCurrentGarde;
	public TextMeshProUGUI argentCurrentVoleur;
	private int nbVole;
	public int nbDefendu = 8;

    private bool GobjetOnce = true;
	static bool Gobjet0 = false;
	static bool Gobjet1 = false;
	static bool Gobjet2 = false;
	static bool Gobjet3 = false;
	static bool Gobjet4 = false;
	static bool Gobjet5 = false;
	static bool Gobjet6 = false;
	static bool Gobjet7 = false;

	public GameObject objet0;
	[SerializeField] int prix0;
	public GameObject objet1;
	[SerializeField] int prix1;
	public GameObject objet2;
	[SerializeField] int prix2;
	public GameObject objet3;
	[SerializeField] int prix3;
	public GameObject objet4;
	[SerializeField] int prix4;
	public GameObject objet5;
	[SerializeField] int prix5;
	public GameObject objet6;
	[SerializeField] int prix6;
	public GameObject objet7;
	[SerializeField] int prix7;

	public TextMeshProUGUI finalScoreVoleurs;
	public TextMeshProUGUI finalScoreGardes;


    void Update(){
		if(SceneManager.GetActiveScene().buildIndex == 2 && GobjetOnce ){
			if(Gobjet0){
				objet0.SetActive(true);
				nbVole++;
				nbDefendu--;
			}
			if(Gobjet1){
				objet1.SetActive(true);
				nbVole++;
				nbDefendu--;
			}
			if(Gobjet2){
				objet2.SetActive(true);
				nbVole++;
				nbDefendu--;
			}
			if(Gobjet3){
				objet3.SetActive(true);
				nbVole++;
				nbDefendu--;
			}
			if(Gobjet4){
				objet4.SetActive(true);
				nbVole++;
				nbDefendu--;
			}
			if(Gobjet5){
				objet5.SetActive(true);
				nbVole++;
				nbDefendu--;
			}
			if(Gobjet6){
				objet6.SetActive(true);
				nbVole++;
				nbDefendu--;
			}
			if(Gobjet7){
				objet7.SetActive(true);
				nbVole++;
				nbDefendu--;
			}
			finalScoreGardes.text = nbDefendu.ToString("0");
			finalScoreVoleurs.text = nbVole.ToString("0");
            GobjetOnce = false;
		} else {
			argentCurrentGarde.text = scoreGardes.ToString("00 000")+"$";
			argentCurrentVoleur.text = scoreVoleurs.ToString("0")+"$";
		}


		
	}

	void OnCollisionEnter(Collision col){
		if(objet0 != null){
			if(col.gameObject.name == objet0.transform.name){
				Destroy(col.gameObject);
				scoreGardes -= prix0;
				scoreVoleurs += prix0;
				Gobjet0 = true;
			}
		}
		if(objet1 != null){
			if(col.gameObject.name == objet1.transform.name){
				Destroy(col.gameObject);
				scoreGardes -= prix1;
				scoreVoleurs += prix1;
				Gobjet1 = true;
			}
		}
		if(objet2 != null){
			if(col.gameObject.name == objet2.transform.name){
				Destroy(col.gameObject);
				scoreGardes -= prix2;
				scoreVoleurs += prix2;
				Gobjet2 = true;
			}
		}
		if(objet3 != null){
			if(col.gameObject.name == objet3.transform.name){
				Destroy(col.gameObject);
				scoreGardes -= prix3;
				scoreVoleurs += prix3;
				Gobjet3 = true;
			}
		}
		if(objet4 != null){
			if(col.gameObject.name == objet4.transform.name){
				Destroy(col.gameObject);
				scoreGardes -= prix4;
				scoreVoleurs += prix4;
				Gobjet4 = true;
			}
		}
		if(objet5 != null){
			if(col.gameObject.name == objet5.transform.name){
				Destroy(col.gameObject);
				scoreGardes -= prix5;
				scoreVoleurs += prix5;
				Gobjet5 = true;
			}
		}
		if(objet6 != null){
			if(col.gameObject.name == objet6.transform.name){
				Destroy(col.gameObject);
				scoreGardes -= prix6;
				scoreVoleurs += prix6;
				Gobjet6 = true;
			}
		}
		if(objet7 != null){
			if(col.gameObject.name == objet7.transform.name){
				Destroy(col.gameObject);
				scoreGardes -= prix7;
				scoreVoleurs += prix7;
				Gobjet7 = true;
			}
		}
	}
}
