﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class tempsJeu : MonoBehaviour
{
    public static float timer;
    static int scoreFinalVoleurs;
    static int scoreFinalGardes;
	public TextMeshProUGUI countdownTextVoleur;
	public TextMeshProUGUI countdownTextGardes;
	public int tempMax = 300;

	public bool cooldown = false;
	public bool cooldownC = true;

	public GameObject porte01;
	public GameObject porte02;
	public GameObject porte03;
	public GameObject porte04;
	public GameObject porte05;
	public GameObject porte06;
	public GameObject porte07;
	public GameObject porte08;

	public Image black;
	public Animator anim;

	void Start (){
		cooldown = true;
		cooldownC = true;
	}

	void Update (){
	
		if(SceneManager.GetActiveScene().buildIndex == 2){
			countdownTextVoleur.text = scoreFinalVoleurs.ToString("00 000");
			countdownTextGardes.text = scoreFinalGardes.ToString("0 000");

			if(tempMax <= timer){
				StartCoroutine(Fading());
			} else {
				timer += Time.deltaTime;
				float minutes = Mathf.Floor(timer / 60);
				float seconds = timer%60;
			}

		} else {
			if(tempMax <= timer){
				scoreFinalGardes = GameObject.Find("DropZone").GetComponent<marquerPoints>().scoreGardes;
				scoreFinalVoleurs = GameObject.Find("DropZone").GetComponent<marquerPoints>().scoreVoleurs;

				//SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
				StartCoroutine(Fading());
			
			} else {
				timer += Time.deltaTime;
				float minutes = Mathf.Floor(timer / 60);
				float seconds = timer%60;
 
				 countdownTextVoleur.text = minutes + seconds.ToString(":00");
				 countdownTextGardes.text = countdownTextVoleur.text;
			}

		if (porte01.GetComponent<Renderer>().material.color == new Color(255, 0, 0)){
            cooldownC = false;
        }
		else if (porte02.GetComponent<Renderer>().material.color == new Color(255, 0, 0)){
            cooldownC = false;
        }
		else if (porte03.GetComponent<Renderer>().material.color == new Color(255, 0, 0)){
            cooldownC = false;
        }
		else if (porte04.GetComponent<Renderer>().material.color == new Color(255, 0, 0)){
            cooldownC = false;
        }
		else if (porte05.GetComponent<Renderer>().material.color == new Color(255, 0, 0)){
            cooldownC = false;
        }
		else if (porte06.GetComponent<Renderer>().material.color == new Color(255, 0, 0)){
            cooldownC = false;
        }
		else if (porte07.GetComponent<Renderer>().material.color == new Color(255, 0, 0)){
            cooldownC = false;
        }
		else if (porte08.GetComponent<Renderer>().material.color == new Color(255, 0, 0)){
            cooldownC = false;
        } else {
			cooldownC = true;
		}



		if (porte01.GetComponent<Door_lock>().doorHinge.rotation == Quaternion.Euler(0.0f, 90.0f, 0.0f)){
            cooldown = false;
        }
		else if (porte02.GetComponent<Door_lock>().doorHinge.rotation == Quaternion.Euler(0.0f, 90.0f, 0.0f)){
            cooldown = false;
        }
		else if (porte03.GetComponent<Door_lock>().doorHinge.rotation == Quaternion.Euler(0.0f, 90.0f, 0.0f)){
            cooldown = false;
        }
		else if (porte04.GetComponent<Door_lock>().doorHinge.rotation == Quaternion.Euler(0.0f, 90.0f, 0.0f)){
            cooldown = false;
        }
		else if (porte05.GetComponent<Door_lock>().doorHinge.rotation == Quaternion.Euler(0.0f, 90.0f, 0.0f)){
            cooldown = false;
        }
		else if (porte06.GetComponent<Door_lock>().doorHinge.rotation == Quaternion.Euler(0.0f, 90.0f, 0.0f)){
            cooldown = false;
        }
		else if (porte07.GetComponent<Door_lock>().doorHinge.rotation == Quaternion.Euler(0.0f, 90.0f, 0.0f)){
            cooldown = false;
        }
		else if (porte08.GetComponent<Door_lock>().doorHinge.rotation == Quaternion.Euler(0.0f, 90.0f, 0.0f)){
            cooldown = false;
        } else {
			cooldown = true;
		}
		
		}
	 }

	 IEnumerator Fading(){
	 	 anim.SetBool("Fade", true);
		 yield return new WaitUntil(()=>black.color.a==1);
		 SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	 }
}
