﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_switch : MonoBehaviour {

    public GameObject camera1;
	public KeyCode btnCam1;
    public GameObject camera2;
	public KeyCode btnCam2;
    public GameObject camera3;
	public KeyCode btnCam3;
    public GameObject camera4;
	public KeyCode btnCam4;
    public GameObject camera5;
	public KeyCode btnCam5;
    public GameObject camera6;
	public KeyCode btnCam6;
    public GameObject camera7;
	public KeyCode btnCam7;
    public GameObject camera8;
	public KeyCode btnCam8;
    public GameObject camera9;
	public KeyCode btnCam9;

	public AudioClip OnMusicClip;
	public AudioSource OnMusicSource;
	public AudioClip OffMusicClip;
	public AudioSource OffMusicSource;


    AudioListener camera1AudioLis;
    AudioListener camera2AudioLis;
    AudioListener camera3AudioLis;
    AudioListener camera4AudioLis;
    AudioListener camera5AudioLis;
    AudioListener camera6AudioLis;
    AudioListener camera7AudioLis;
    AudioListener camera8AudioLis;
    AudioListener camera9AudioLis;



    // Use this for initialization
    void Start()
    {

        //Get Camera Listeners
        camera1AudioLis = camera1.GetComponent<AudioListener>();
        camera2AudioLis = camera2.GetComponent<AudioListener>();
        camera3AudioLis = camera3.GetComponent<AudioListener>();
        camera4AudioLis = camera4.GetComponent<AudioListener>();
        camera5AudioLis = camera5.GetComponent<AudioListener>();
        camera6AudioLis = camera6.GetComponent<AudioListener>();
        camera7AudioLis = camera7.GetComponent<AudioListener>();
        camera8AudioLis = camera8.GetComponent<AudioListener>();
        camera9AudioLis = camera9.GetComponent<AudioListener>();

		OnMusicSource.clip = OnMusicClip;
		OffMusicSource.clip = OffMusicClip;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(btnCam1))
        {
			desactivateAll();
            camera1.SetActive(true);
            camera1AudioLis.enabled = true;
			OnMusicSource.Play();
        }

         if (Input.GetKeyDown(btnCam2))
        {
			desactivateAll();
            camera2.SetActive(true);
            camera2AudioLis.enabled = true;
			OnMusicSource.Play();
        }

         if (Input.GetKeyDown(btnCam3))
        {
			desactivateAll();
            camera3.SetActive(true);
            camera3AudioLis.enabled = true;
			OnMusicSource.Play();
        }

         if (Input.GetKeyDown(btnCam4))
        {
			desactivateAll();
            camera4.SetActive(true);
            camera4AudioLis.enabled = true;
			OnMusicSource.Play();
        }

         if (Input.GetKeyDown(btnCam5))
        {
			desactivateAll();
            camera5.SetActive(true);
            camera5AudioLis.enabled = true;
			OnMusicSource.Play();
        }

         if (Input.GetKeyDown(btnCam6))
        {
			desactivateAll();
            camera6.SetActive(true);
            camera6AudioLis.enabled = true;
			OnMusicSource.Play();
        }

         if (Input.GetKeyDown(btnCam7))
        {
			desactivateAll();
            camera7.SetActive(true);
            camera7AudioLis.enabled = true;
			OnMusicSource.Play();
        }

         if (Input.GetKeyDown(btnCam8))
        {
			desactivateAll();
            camera8.SetActive(true);
            camera8AudioLis.enabled = true;
			OnMusicSource.Play();
        }

         if (Input.GetKeyDown(btnCam9))
        {
			desactivateAll();
            camera9.SetActive(true);
            camera9AudioLis.enabled = true;
			OnMusicSource.Play();
        }

    }

	void desactivateAll(){
		OnMusicSource.Play();
		camera1.SetActive(false);
		camera2.SetActive(false);
		camera3.SetActive(false);
		camera4.SetActive(false);
		camera5.SetActive(false);
		camera6.SetActive(false);
		camera7.SetActive(false);
		camera8.SetActive(false);
        camera9.SetActive(false);

	}
}