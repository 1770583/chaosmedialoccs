﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;
using System.Collections;
 
public class Door_lock : MonoBehaviour {

    /*
     
    Toutes les variables requises      
         
    */
 
    public bool doorOpen;
    public Transform doorHinge;
	public bool blue;
    public KeyCode btnBarrer = KeyCode.P;
    public KeyCode btnOuvrir = KeyCode.O;
    static float countdown = 3.0f;
	static float countdownC = 3.0f;

	public AudioClip LockMusicClip;
	public AudioSource LockMusicSource;


    /*
     
    Initialisation au départ
    de toutes les variables et de la couleur de base
         
    */

	void Start(){
	       doorOpen = false;
		   blue = true;
		   GetComponent<Renderer>().material.color = new Color(0, 0, 255);

		   LockMusicSource.clip = LockMusicClip;
	}
 
    void Update()
    {
        /*
         
        Code pour ouvrir la porte

         */
        if(Input.GetKeyDown(btnOuvrir) && GetComponent<Renderer>().material.color == new Color(0, 0, 255) && !doorOpen && GameObject.Find("Jeu").GetComponent<tempsJeu>().cooldown)
        {
			if(doorOpen){
				doorOpen = false;
			} else {
				doorOpen = true;
				if(countdown <3.0f){
				countdown = 3.0f;
				}
			}
        }
        Switch();

        /*
         
         Code pour barrer la porte
         
         */

		if (Input.GetKeyDown(btnBarrer) && blue && GameObject.Find("Jeu").GetComponent<tempsJeu>().cooldownC){
			LockMusicSource.Play();
            blue = false;
            Lock();
            if (countdownC < 3.0f){
                countdownC = 3.0f;
            }
        }

        /*
         
         Vérification que la porte est ouverte
         pour partir le cooldown
         
         */

        if (doorOpen)
        {
            countdown -= Time.deltaTime;
            if (countdown <= 0.0f)
                doorOpen = false;
                Switch();

        }

        /*
         
         Vérification que la porte soit barré
         pour partir le cooldown
         
         */

        if (!blue){
			countdownC -= Time.deltaTime;
			if(countdownC <= 0.0f)
				blue = true;
                Lock();
        }
    }
	

    /*
     
    Fonction pour bouger la porte vers le mode ouvert ou fermé
    selon son état actuel     
         
    */
	void Switch(){
			if(doorOpen){
				var newRot = Quaternion.RotateTowards(doorHinge.rotation, Quaternion.Euler(0.0f, 90.0f, 0.0f), Time.deltaTime * 250);
				doorHinge.rotation = newRot;
			} else {
				var newRot = Quaternion.RotateTowards(doorHinge.rotation, Quaternion.Euler(0.0f, 0.0f, 0.0f), Time.deltaTime * 250);
				doorHinge.rotation = newRot;
			}
		}

    /*
     
    Fonction pour que la couleur de la porte change selon l'état requis     
         
    */

	void Lock(){
		if (blue){
            GetComponent<Renderer>().material.color = new Color(0, 0, 255);
        } else {
            GetComponent<Renderer>().material.color = new Color(255, 0, 0);
		}
	}
}